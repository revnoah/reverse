# Issue Template

## Description

Please include a summary of the issue. 

ReplaceThisText

## Type of issue

- [ ] Weird behaviour (it did something unexpected)
- [ ] Bad output (it output is functional but doesn't look good)
- [ ] Confusing interface (it has a user interface problems that makes it difficult to use)
- [ ] Broken (crashes)
- [ ] New feature or improvement
