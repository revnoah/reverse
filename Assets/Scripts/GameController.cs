﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;

public class GameController : MonoBehaviour {
    //public variables may be overridden in Unity IDE
    public int maxItems = 9;
    public float startingTargetTime = 10.0f;
    public float targetTime = 10.0f;
    public float speedMultiplier = 1.2f;
    private float lastTime = 0.0f;
    public int score = 0;
    public int scoreMinimum = 100;
    public int scoreBonus = 0;
    public int scoreBonusMultiplier = 100;
    public int scoreBonusDecrementer = 10;
    public int gameRound = 0;
    public bool gamePaused = true;
    private int scoreMultiplier = 100;
    private Button messagePanelButton;

    //message related stuff
    public string sMessageDefault = "";
    public string sMessageWin = "Finished Round";
    public string sMessageLose = "You Lose";
    public string sMessageStart = "Get Ready";
    public string sMessageStartSub = "Reorder Numbers In Fewest Moves Possible";
    public string sMessageSubNextRound = "Touch For Next Round";
    public string sMessageSubNewGame = "Touch For New Game";

    // Use this for initialization
    void Start () {
        //instantiate handlers and additional one-time declarations
        SetupItems();
    }

    //play new round or start new game
    public void PlayGame()
    {
        if(gameRound == 0)
        {
            StartNewGame();
        } else
        {
            StartNewRound();
        }
    }

    //start new game
    private void StartNewGame()
    {
        score = 0;
        gameRound = 1;
        gamePaused = false;

        //set starting time to default for first round
        targetTime = startingTargetTime;

        //set score bonus, based on multiplier and round
        ResetBonusScore();

        //build random set of units
        RandomizeItems();
    }

    private void StartNewRound()
    {
        //increment round number
        gameRound++;
        gamePaused = false;

        //reset starting time to preset, minus our round
        targetTime = startingTargetTime - (float)gameRound;

        //set score bonus, based on multiplier and round
        ResetBonusScore();

        //build random set of units
        RandomizeItems();
    }

    // Update is called once per frame
    void Update()
    {
        //if the timer runs out and the user is playing a game
        if (targetTime <= 0.0f && gameRound > 0 && !gamePaused)
        {
            //execute game lost state and message
            GameLost();
        }
        else if (targetTime > 0.0f && !gamePaused)
        {
            //subtract the framedraw execution time
            targetTime -= Time.deltaTime;

            //it is possible to get a negative value
            if (targetTime < 0.0f)
            {
                targetTime = 0.0f;
            }

            //update the display of the score
            UpdateTimer(targetTime);
        }
    }

    //display out-of-time message to the user and update game state
    private void GameLost()
    {
        gameRound = 0;
        //TODO: show loser message
        //EditorUtility.DisplayDialog("You Lose!", "You ran out of time", "Fail");

        //TODO: present option to start new game
        //StartNewGame()
        //Button button = GameObject.Find("NewGameButton").GetComponent<Button>();
        //button.interactable = true;
        //button.onClick.AddListener(() => StartNewGame());

        //TODO: show NewGameButton
        //Text messagePanelButtonText = GameObject.Find("MessageOverlayText").GetComponent<Text>();

        //messagePanelButton = GameObject.Find("MessageOverlayButton").GetComponent<Button>();
        //messagePanelButton.interactable = true;
        //messagePanelButton.onClick.AddListener(() => MessageButtonClick(messagePanelButton));

        //set message text
        SetMessage("lose");
    }

    //display won-game message
    private void GameWon()
    {
        //update score
        score += scoreBonus * (int)targetTime;
        UpdateScore();

        //set gameplay status to paused
        gamePaused = true;

        //update message
        Debug.Log("You won! Awesome!");
        SetMessage("win");
    }

    //update score text display
    private void UpdateScore()
    {
        Text scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        scoreText.text = score.ToString("00000");

        Debug.Log("Score: " + score.ToString() + " " + score.ToString("000000"));
    }

    //reset bonus score for countdown
    private void ResetBonusScore()
    {
        //set score bonus, based on multiplier and round
        //scoreBonus = scoreBonusMultiplier * gameRound * scoreMultiplier;
        scoreBonus = scoreBonusMultiplier * gameRound;
    }

    //update score text display
    private void UpdateBonus()
    {
        Debug.Log("Update Bonus");

        //default is to reduce bonus with each turn
        //scoreBonus -= (scoreBonusMultiplier * (int)Time.deltaTime / 10);
        scoreBonus -= (scoreBonusDecrementer * gameRound);

        //update bonus and set to minimum
        if (lastTime - targetTime < 1.0f)
        {
            Debug.Log("Speed Bonus");

            //add speed bonus
            //scoreBonus -= (scoreBonusMultiplier * 10 * gameRound);
            //scoreBonus -= (scoreBonusMultiplier * (int)Time.deltaTime / 10);
        }

        /*
        if (scoreBonus > scoreMinimum)
        {
            Debug.Log("Normal");

            //normal amount
            scoreBonus -= (scoreBonusMultiplier * (int)Time.deltaTime / 10);
        } else
        {
            Debug.Log("Else - Default");

            //default is to reduce bonus with each turn
            //scoreBonus -= (scoreBonusMultiplier * (int)Time.deltaTime / 10);
            scoreBonus -= (scoreBonusMultiplier * gameRound);
        }
        */

        Debug.Log("Score bonus updated: " + scoreBonus);

        //TODO: reenable minimum
        /*
        //base minimum score
        if (scoreBonus < scoreMinimum)
        {
            scoreBonus = scoreMinimum;
        }
        */

        //update display of bonus text
        Text bonusText = GameObject.Find("BonusText").GetComponent<Text>();
        bonusText.text = scoreBonus.ToString("00000");

        //update lastTime to track speed bonus
        lastTime = targetTime;
    }

    //update timer text display
    private void UpdateTimer(float time)
    {
        Text timeText = GameObject.Find("TimeText").GetComponent<Text>();
        timeText.text = time.ToString("0.00");
    }

    //main game setup
    private void SetupItems()
    {
        for (int i = 1; i < (maxItems + 1); i++)
        {
            System.Random rnd = new System.Random();
            int randomIndex = rnd.Next(1, maxItems);

            //get the NumberButton prefab already added to the gameobject
            NumberButton button = GameObject.Find("NumberButton" + i).GetComponent<NumberButton>();

            //set text display and set internal variable
            button.GetComponentInChildren<Text>().text = i.ToString();
            button.setNumberIndex(i);

            //update random index - note, this will shift position of items each time but we're looking for random anyway
            button.transform.SetSiblingIndex(randomIndex);

            //find the Button inside the NumberButton parent object
            Button b = button.GetComponent<Button>();

            //add click listener to handler reordering of button, prefab has its own cs file
            //pass in button so that the index can be calculated after reposition
            b.onClick.AddListener(() => ButtonClick(button));
        }

        //setup overlay handler
        messagePanelButton = GameObject.Find("MessageOverlayButton").GetComponent<Button>();
        messagePanelButton.interactable = true;
        messagePanelButton.onClick.AddListener(() => MessageButtonClick(messagePanelButton));

        //display start game message
        SetMessage("start");

        //init remaining game variables
        gameRound = 0;
        gamePaused = true;
    }

    //handle start/stop game events, message confirmation
    private void MessageButtonClick(Button messagePanelButton)
    {
        //if game is paused, unpause it
        if (gameRound == 0)
        {
            //start new game
            StartNewGame();
        }
        else if (gamePaused && gameRound > 0)
        {
            //start new round
            StartNewRound();
        }

        //update game state
        gamePaused = false;

        //hide button
        messagePanelButton.gameObject.SetActive(false);
    }

    //overloaded message for message, default to visible messages
    private void SetMessage(string messageState)
    {
        SetMessage(messageState, true);
    }

    //set message text
    private void SetMessage(string messageState, bool visibility)
    {
        //set visibility of button, usually true
        messagePanelButton.gameObject.SetActive(visibility);

        //update message text
        string messageText = "";
        string messageTextSub = "";
        switch (messageState)
        {
            case "win":
                messageText = sMessageWin + " " + gameRound.ToString();
                messageTextSub = sMessageSubNextRound;
                break;
            case "lose":
                messageText = sMessageLose;
                messageTextSub = sMessageSubNewGame;
                break;
            case "start":
                messageText = sMessageStart;
                messageTextSub = sMessageStartSub;
                break;
            case "default":
            default:
                messageText = sMessageDefault;
                messageTextSub = sMessageSubNewGame;
                break;
        }

        //update message text
        Text messagePanelText = GameObject.Find("MessageOverlayText").GetComponent<Text>();
        messagePanelText.text = messageText;

        //update message sub text
        Text messagePanelTextSub = GameObject.Find("MessageOverlayTextSub").GetComponent<Text>();
        messagePanelTextSub.text = messageTextSub;
    }

    //randomize items, or something close to random
    private void RandomizeItems()
    {
        //flag to ensure random numbers are sufficiently randomized
        bool newRandomNumber = true;

        while (newRandomNumber)
        {
            //create new collection of NumberButtons
            NumberButton[] button = new NumberButton[maxItems];

            //loop through each item and set a random position
            for (int i = 1; i < (maxItems + 1); i++)
            {
                System.Random rnd = new System.Random();
                int randomIndex = rnd.Next(1, maxItems);

                //get the NumberButton prefab already added to the gameobject
                NumberButton tempButton = GameObject.Find("NumberButton" + i).GetComponent<NumberButton>();

                //update random index - note, this will shift position of items each time but we're looking for random anyway
                tempButton.transform.SetSiblingIndex(randomIndex);
            }

            //add to collection after resorting is complete
            for (int i = 1; i < (maxItems + 1); i++)
            {
                //get the NumberButton prefab already added to the gameobject
                NumberButton tempButton = GameObject.Find("NumberButton" + i).GetComponent<NumberButton>();

                //add to collection, should start at zero
                button[i - 1] = tempButton;
            }

            //ensure that we did not randomly sort the list
            if (!ItemsMatch(button))
            {
                newRandomNumber = false;
            }
        }

    }

    //handle button click actions
    private void ButtonClick(NumberButton thisButton)
    {
        //if game is paused, unpause it
        if(gameRound == 0)
        {
            //update game state
            gamePaused = false;

            //start new game
            StartNewGame();
        } else if(gamePaused && gameRound > 0)
        {
            //update game state
            gamePaused = false;

            //start new round
            StartNewRound();
        } else
        {
            //sort items
            SortItems(thisButton);

            //update bonus
            UpdateBonus();
        }
    }

    private void SortItems(NumberButton thisButton)
    {
        int endingPosition = thisButton.transform.GetSiblingIndex();
        int[] newPosition = new int[maxItems];
        NumberButton[] button = new NumberButton[maxItems];

        for (int i = 0; i < maxItems; i++)
        {
            //this relies on these objects being added and named
            //NOTE: there is probably a more efficient way of selecting these
            NumberButton tempButton = GameObject.Find("NumberButton" + (i + 1)).GetComponent<NumberButton>();

            //add to button array and get the current index of the button
            button[i] = tempButton;
            int buttonSiblingIndex = tempButton.transform.GetSiblingIndex();

            //only bother setting the position
            if(buttonSiblingIndex <= endingPosition)
            {
                //the game logic involves flipping the numbers up until a position chosen by the player
                newPosition[i] = endingPosition - buttonSiblingIndex;
            }
        }

        //now loop through the buttons and update applicable indices
        for (int i = button.Length - 1; i >= 0; i--)
        {
            int buttonSiblingIndex = button[i].transform.GetSiblingIndex();

            //only manipulating siblings up to a point
            if (buttonSiblingIndex <= endingPosition)
            {
                button[i].transform.SetSiblingIndex(newPosition[i]);
            }
        }

        //see if button array siblings match their set value plus one
        if(ItemsMatch(button))
        {
            GameWon();
        }
    }

    //helper function to check if items match
    private bool ItemsMatch(NumberButton[] checkButtons)
    {
        bool itemsMatch = true;
        for(int i = 0; i < checkButtons.Length; i++)
        {
            if((checkButtons[i].transform.GetSiblingIndex() + 1) != checkButtons[i].GetNumberIndex())
            {
                itemsMatch = false;
                break;
            }
        }

        return itemsMatch;
    }
}
