﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item {
    private int value;

    public void SetValue(int newValue)
    {
        value = newValue;
    }

    public int GetValue()
    {
        return value;
    }
}
