﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
using UnityEngine.UI;

public class NumberButton : MonoBehaviour {

    public int numberIndex;
    public Text buttonText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //getters and setters
    public void setNumberIndex(int idx)
    {
        numberIndex = idx;
        //var button = Selection.activeGameObject;
        //button.GetComponentInChildren<Text>().text = idx.ToString();
    }

    public int GetNumberIndex()
    {
        return numberIndex;
    }

    //button action
    public void ButtonClickAction()
    {
        //EditorUtility.DisplayDialog("Button Selected", "Number: " + numberIndex.ToString(), "Ok");
    }
}
