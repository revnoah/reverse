﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MessageOverlayController : MonoBehaviour {

    public string messageText;
    public bool messageVisibility = false;

    private string sMessageDefault = "";
    private string sMessageWin = "You Win";
    private string sMessageLose = "You Lose";
    private string sMessageStart = "Get Ready";

    public void ConfirmMessage()
    {
        if(messageVisibility)
        {
            //clear message text
            messageText = sMessageDefault;

            //hide message and overlay
            messageVisibility = false;
        }
    }

    private void SetMessage(string messageState)
    {
        SetMessage(messageState, true);
    }

    private void SetMessage(string messageState, bool visibility)
    {
        //update message text
        switch(messageState)
        {
            case "win":
                messageText = sMessageWin;
                break;
            case "lose":
                messageText = sMessageLose;
                break;
            case "start":
                messageText = sMessageStart;
                break;
            case "default":
            default:
                messageText = sMessageDefault;
                break;
        }

        //update visibility of message
        messageVisibility = visibility;
    }

}
