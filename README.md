# Reverse

## Porting A Game From BASIC To Unity

**Reverse** is a simple number sorting game, ported from a BASIC application published by Creative Computing.

In this project, I port the game concepts from BASIC to Unity2D. Since I have only novice level skills with Unity, this limited my approach. 


## Development

This project was started as an experiment. Development will probably not continue.


## Contributing

But if you really want to work on this, please review the [CONTRIBUTING.md](CONTRIBUTING.md). 
Also, please be aware that contributors are expected to adhere to the 
[CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md) and use the [PULL_REQUEST_TEMPLATE.md](PULL_REQUEST_TEMPLATE.md) when submitting code.


## Authors

Reverse for Unity was written by Noah J. Stewart. It is based on Reverse for Atari written by Peter Sessions.


## Links

https://www.atariarchives.org/bcc1/showpage.php?page=258


## License

The WordPress plugin **Enhanced Body Class** is open-sourced software licensed under the ISC license.
