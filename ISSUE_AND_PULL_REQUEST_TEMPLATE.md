# Issue and Pull Request Template

## Description

Please include a summary of the issue and the fix. Please also include relevant motivation and context.

ReplaceThisText

## Type of issue

- [ ] Weird behaviour (it did something unexpected)
- [ ] Bad output (it output is functional but doesn't look good)
- [ ] Confusing interface (it has a user interface problems that makes it difficult to use)
- [ ] Broken (crashes)
- [ ] New feature or improvement

## Type of change

- [ ] Bug fix (non-breaking change which fixes an issue)
- [ ] New feature (non-breaking change which adds functionality)
- [ ] This change requires a documentation update

## Checklist:

- [ ] My code follows the style guidelines of this project
- [ ] I have performed a self-review of my own code
- [ ] I have commented my code, with docblocks for function declarations and single-line comments and spacing for blocks of code
- [ ] I have made corresponding changes to the documentation
- [ ] My changes generate no new warnings
- [ ] Any dependent changes have been merged and published in downstream modules
